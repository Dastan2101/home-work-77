const express = require('express');
const multer = require('multer');
const path = require('path');
const config = require('../config');
const router = express.Router();
const fileMessages = require('../database');
const nanoid = require('nanoid');


const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath)
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});


router.post('/', upload.single('image'), (req, res) => {

    const comment = req.body;

    const date = new Date().toISOString();


    if (req.file) {
        comment.image = req.file.filename;
    }

    if (comment.message === '') {
        res.status(400).send({error: "Введите пожалуйста сообщение !"});

    } else {
        const author = comment.author.trim();
        if (!author) {
            comment.author = "anonymous";
        }

        comment.datetime = date.slice(0, -5);
        fileMessages.addComment(comment);

        res.send(comment)
    }

});

router.get('/', (req, res) => {
    res.send(fileMessages.getComments());
});


module.exports = router;