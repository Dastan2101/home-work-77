const express = require('express');
const cors = require('cors');
const comments = require('./app/comments');
const app = express();
const fileMessages = require('./database');
fileMessages.init();

app.use(express.json());
app.use(express.static('public'));
app.use(cors());

const port = 7000;

app.use('/comments', comments);

app.listen(port);