const fs = require('fs');

const commentsFile = './comments.json';

let data = [];

module.exports = {
    init() {
        try {
            const fileContent = fs.readFileSync(commentsFile);
            data = JSON.parse(fileContent);
        } catch (e) {
            data = []
        }
    },
    getComments() {
        return data;
    },

    addComment(com) {
        data.push(com);
        this.save()
    },
    save() {
        fs.writeFileSync(commentsFile, JSON.stringify(data, null, 2))
    }
};