import axios from '../../axios-tred';

export const FETCH_COMMENTS_SUCCESS = 'FETCH_COMMENTS_SUCCESS';

export const createCommentError = error => ({type: createCommentError, error});

export const getCommentSuccess = data => ({type: FETCH_COMMENTS_SUCCESS, data});
export const createComment = data => {
    return dispatch => {
        axios.post('comments/', data).then(
            response => {
                dispatch(getCommentSuccess(response.data))
            }
        )
    }
};

export const getComment = () => {
    return dispatch => {
        axios.get('/comments').then(
            response => dispatch(getCommentSuccess(response.data))
        )
    }
};