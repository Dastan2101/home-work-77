import {FETCH_COMMENTS_SUCCESS} from "../actions/actionTypes";

const initialState = {
    comments: [],
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_COMMENTS_SUCCESS:
            return{
                ...state, comments: [...state.comments].concat(action.data)
            };

        default:
            return state;
    }
};

export default reducer