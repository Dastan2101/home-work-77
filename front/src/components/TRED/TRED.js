import React, {Component, Fragment} from 'react';
import {createComment, getComment} from "../../store/actions/actionTypes";
import {connect} from "react-redux";
import CommentThumbnail from "../CommentThumbNail/CommentThumbNail";
import './Tred.css';

class Tred extends Component {

    state = {
        author: '',
        text: '',
        image: ''
    };

    componentDidMount() {
        this.props.getComment();
        // some
    }

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    submitFormHandler = (event) => {
        event.preventDefault();

        const formData = new FormData();

        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key])
        });

        this.props.createComment(formData);

    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        });
    };



    render() {
        let comments = this.props.comments.map((comment, indx) => (
            <div key={indx} className="comment-div">
                <span>{comment.author}</span>
                <span>{comment.datetime}</span>
                <p><i>&#10077;</i> {comment.text} <i>&#10078;</i></p>
                <CommentThumbnail image={comment.image}/>

            </div>
        ));

        return (
            <Fragment>
                <div>
                    <div className="comments">
                        {comments}
                    </div>
                    <div className="form">
                        <form onSubmit={this.submitFormHandler}>
                            <div style={{display: 'flex', justifyContent: 'space-between'}}>
                                <input type="text" placeholder="автор" name="author"
                                       onChange={this.inputChangeHandler}/>
                                <button type="submit">Отправить</button>

                            </div>
                            <div>
                                <textarea placeholder="сообщение" required name="text"
                                          onChange={this.inputChangeHandler}/>
                                <input type="file" name="image" onChange={this.fileChangeHandler}/>

                            </div>


                        </form>
                    </div>
                </div>
            </Fragment>

        );
    }
}

const mapStateToProps = state => ({
    comments: state.comments
});

const mapDispatchToProps = dispatch => ({
    createComment: data => dispatch(createComment(data)),
    getComment: () => dispatch(getComment())
});

export default connect(mapStateToProps, mapDispatchToProps)(Tred);