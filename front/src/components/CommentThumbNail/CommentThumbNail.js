import React from 'react';

import {apiUrl} from "../../constants";

const styles = {
    width: '100px',
    height: '100px',
    marginRight: '10px'
};

const notStyle = {
    width: "0",
    height: "0"
};

const CommentThumbnail = (props) => {

    let image = null;

    if (props.image !== "") {
        image = apiUrl + '/uploads/' + props.image;
    }

    return <img src={image} style={image ? styles : notStyle} className="img-thumbnail" alt=""/>
};

export default CommentThumbnail;